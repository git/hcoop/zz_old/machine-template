#!/bin/bash

# Run on newly created node as a kerberos/afs admin with local sudo rights

set -e
set -v
set -x

# Misc postinst stuff
# Unfortunately has to be run after the first reboot by an admin user

# Extract host keytab
sudo kadmin -p ${USER}@HCOOP.NET -r HCOOP.NET -q "ktadd -k /etc/krb5.keytab host/outpost.hcoop.net@HCOOP.NET"
sudo chown root:root /etc/krb5.keytab
sudo chmod go-rwx /etc/krb5.keytab

# Sync initial set of keytabs
sudo mkdir -p /etc/keytabs

(ssh fritz.hcoop.net cd /etc/keytabs \; sudo tar clpf - . | \
     (cd /etc/keytabs; sudo tar xlpf -))

# deploy domtool locally
sudo touch /var/log/domtool.log
sudo chown domtool:nogroup /var/log/domtool.log
sudo chmod 600 /var/log/domtool.log

sudo mkdir -p /var/domtool
sudo chown domtool:nogroup /var/domtool
sudo chmod 755 /var/domtool

#sudo mkdir -p /var/log/apache2
#sudo mkdir -p /var/log/apache2/user
#sudo chown domtool:nogroup /var/log/apache2/user
#sudo chmod 755 /var/log/apache2/user
#sudo -u domtool mkdir -p /var/domtool/vhosts

sudo -u domtool touch /var/domtool/local_domains.cfg

sudo -u domtool mkdir -p /var/domtool/firewall
sudo -u domtool mkdir -p /var/domtool/zones

# FIXME: move this script to the common scripts volume
~clinton_admin/deploy-domtool-on-host.sh --slave --bootstrap

fwtool regen outpost

# Basic Packages Needed for Web Serving
#sudo apt-get install apache2-mpm-prefork hcoop-apache2-config

echo "Manually run 'domtool-admin regen' if needed"