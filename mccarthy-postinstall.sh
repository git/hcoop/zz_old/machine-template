#!/bin/bash

# FIXME BEFORE REINSTALLING Override /etc/adduser.conf to set
# min_system_uid/gid to 110 to skip over domtool. We are going to kill
# the domtool identity for domtool2 eventually, but...

# Overriding in hcoop-common-config is sufficient, as long as fewer
# than 8 packages requesting dynamically allocated system uids are
# installed by d-i

# Run on newly created node as a kerberos/afs admin with local sudo rights

set -e
set -v
set -x

# Misc postinst stuff
# Unfortunately has to be run after the first reboot by an admin user

# Domtool was created in pts with id #108, so we skip to 110 for
# dynamically allocated UID/GIDs to avoid conflict until the domtool
# pts user can be replaced.
sudo su -c "cat /etc/adduser.conf | sed -e 's/^FIRST_SYSTEM_UID=.*/FIRST_SYSTEM_UID=110/' -e 's/^FIRST_SYSTEM_GID=.*/FIRST_SYSTEM_GID=110/' > /etc/adduser.conf.hcoop"
sudo mv /etc/adduser.conf /etc/adduser.conf.hcoop-orig
sudo ln -s /etc/adduser.conf.hcoop /etc/adduser.conf

sudo apt-get install hcoop-admin-common-config

# Extract host keytab
sudo kadmin -p ${USER}@HCOOP.NET -r HCOOP.NET -q "ktadd -k /etc/krb5.keytab host/mccarthy.hcoop.net@HCOOP.NET"
sudo chown root:root /etc/krb5.keytab
sudo chmod go-rwx /etc/krb5.keytab

# Sync initial set of keytabs
sudo mkdir -p /etc/keytabs
sudo mkdir -p /etc/keytabs/service

(ssh fritz.hcoop.net cd /etc/keytabs \; sudo tar clpf - domtool hcoop | \
     (cd /etc/keytabs; sudo tar xlpf -))

# Web Server
sudo kadmin -p ${USER}@HCOOP.NET -r HCOOP.NET -q "ktadd -k /etc/keytabs/service/apache apache2/mccarthy.hcoop.net@HCOOP.NET"
sudo chown www-data:root /etc/keytabs/service/apache
sudo chmod 400 /etc/keytabs/service/apache

sudo addgroup hcoop-tlscert

(ssh navajos.hcoop.net sudo tar clpf - /etc/hcoop-ssl/hcoop.pem | \
     (cd /; sudo tar xlpf -))

# Just in case
sudo chown root:hcoop-tlscert /etc/hcoop-ssl
sudo chown root:hcoop-tlscert /etc/hcoop-ssl/hcoop.pem

sudo chmod 750 /etc/hcoop-ssl
sudo chmod 440 /etc/hcoop-ssl/hcoop.pem

# deploy domtool locally
sudo touch /var/log/domtool.log
sudo chown domtool:nogroup /var/log/domtool.log
sudo chmod 600 /var/log/domtool.log

sudo mkdir -p /var/domtool
sudo chown domtool:nogroup /var/domtool
sudo chmod 755 /var/domtool

sudo -u domtool mkdir -p /var/domtool/vhosts
sudo -u domtool mkdir -p /var/domtool/firewall

/afs/hcoop.net/common/etc/scripts/deploy-domtool-on-host --slave --bootstrap

fwtool regen mccarthy

echo "Manually run 'domtool-admin regen' if needed"
